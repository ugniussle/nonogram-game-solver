# nonogram game solver

A python nonogram game and solver (solver not implemented).

![example](game.png)

# required packages

1. pygame
2. numpy

# running

```python nngGame.py```