import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = ''
import numpy as np
import gym
from gym import spaces
from stable_baselines3 import A2C
from stable_baselines3.common.vec_env.dummy_vec_env import DummyVecEnv
from stable_baselines3.common.evaluation import evaluate_policy
from nngGenerator import NonogramGenerator
from nngGame import BoardRenderer
import time
from stable_baselines3.common.monitor import Monitor

def getRowsInst(matrix:np.ndarray):
    onesLists = list()

    for row in matrix:
        onesList = list()

        count = 0
        for value in row:
            if value == 1:
                count = count + 1

            elif count > 0:
                onesList.append(count)
                count = 0
        
        if count > 0: onesList.append(count)

        onesLists.append(onesList)

    return onesLists

def checkIfSolved(matrix, instructions):
    if getRowsInst(matrix) == instructions[0]:
        print('rows match')
    
        if getRowsInst(np.transpose(matrix)) == instructions[1]:
            print('columns match')

            return True

    return False

def checkReward(cell, matrix, solution):
    try:
        matrix[cell[0], cell[1]]
        solution[cell[0], cell[1]]
    except IndexError:
        return -30

    if matrix[cell[0], cell[1]] == solution[cell[0], cell[1]] and matrix[cell[0], cell[1]] == 1:
        return -20
    if matrix[cell[0], cell[1]] == solution[cell[0], cell[1]]:
        return -10
    else:
        return 10

def instructionsToMatrix(instructions:list, shape:tuple, maxDim) -> np.ndarray:
    inst = list()
    
    for dir in instructions:
        for row in dir:
            inst.append(row)

    instructions = inst

    rows = shape[0] + shape[1]

    columns = int((maxDim / 2) + maxDim % 2)

    matrix = np.zeros((rows, columns), np.int8)

    for row in range(rows):
        for column in range(columns):
            try:
                matrix[row, column] = instructions[row][column]
            except:
                matrix[row, column] = 0

    return matrix

class NonogramEnvironment(gym.Env):
    def __init__(self, dim):
        super(NonogramEnvironment, self).__init__()

        self.dim = dim

        self.maxDim = 0
        for d in dim:
            if d > self.maxDim: self.maxDim = d

        self.reward_range = (-30, 10)

        self.action_space = spaces.Box(0, self.maxDim - 1, (2,), dtype = np.int8)

        self.solutionMatrix = NonogramGenerator().generateMatrix(dim[0], dim[1])

        self.matrix = np.zeros(dim)

        instructions = NonogramGenerator().generateInstructions(self.solutionMatrix)
        self.observation = {
            'instructions': instructionsToMatrix(instructions, self.dim, self.maxDim),
            'matrix': self.matrix
        }
        
        instructionBox = spaces.Box(0, self.maxDim, (self.dim[0] + self.dim[1], int((self.maxDim / 2) + self.maxDim % 2)), np.int8)
        matrixBox = spaces.Box(0, 1, self.dim, np.int8)
        self.observation_space = spaces.Dict({
            'instructions': instructionBox,
            'matrix': matrixBox
        })

        self.board = BoardRenderer((dim[0], dim[1]))
    
    def step(self, action):
        reward = checkReward(action, self.matrix, self.solutionMatrix)

        if reward > 0: self.matrix[action[0], action[1]] = 1

        self.observation['matrix'] = self.matrix

        if not (False in np.equal(self.matrix, self.solutionMatrix)):
            done = True
        else:
            done = False

        if reward < 0: done = True

        info = {}
        
        return self.observation, reward, done, info

    def reset(self):
        self.solutionMatrix = NonogramGenerator().generateMatrix(self.dim[0], self.dim[1])

        self.matrix = np.zeros(self.dim)

        instructions = NonogramGenerator().generateInstructions(self.solutionMatrix)
        self.observation = {
            'instructions': instructionsToMatrix(instructions, self.dim, self.maxDim),
            'matrix': self.matrix
        }

        return self.observation

    def render(self, mode='human'):
        self.board.clock.tick(60)

        self.board.setMatrix(env.matrix)
        self.board.updateWindow()

        time.sleep(0.1)

size = (3, 3)

env = NonogramEnvironment(size)

episodes = 0
for epsiode in range(1, episodes + 1):
    state = env.reset()
    done = False
    score = 0

    while not done:
        action = env.action_space.sample()
        n_state, reward, done, info = env.step(action)

        env.render()

        score += reward
    
    print('Epsiode:', epsiode, 'Score:', score)

""" #model = A2C('MultiInputPolicy', env, verbose = 1)

#model = model.learn(10000)

model = A2C.load('nng-model')
#model.save('nng-model')

print(evaluate_policy(model, Monitor(env), n_eval_episodes = 100, render = True)) """
