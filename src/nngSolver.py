import numpy as np
from nngGenerator import NonogramGenerator
from nngGame import BoardRenderer

class NonogramSolver:
    inst = None
    matrix = None

    def __init__(self, instructions) -> None:
        self.inst = instructions
        self.rows = len(instructions[0])
        self.columns = len(instructions[1])
        self.matrix = np.zeros((self.rows, self.columns))

    def __getRowsInst(self, matrix:np.ndarray):
        onesLists = []

        for row in matrix:
            onesList = []

            count = 0
            for value in row:
                if value == 1:
                    count = count + 1

                elif count > 0:
                    onesList.append(count)
                    count = 0
            
            if count > 0:
                onesList.append(count)

            onesLists.append(onesList)

        print(onesList)

        return onesLists

    def checkIfSolved(self, matrix, instructions):
        if self.__getRowsInst(matrix) == instructions[0]:
            print('rows match')
        
            if self.__getRowsInst(np.transpose(matrix)) == instructions[1]:
                print('columns match')

                return True

        return False

    def isInList(self, value, l):
        for v in l:
            if v == value:
                return True

        return False

    def addInstructionToRow(self, row, instruction, index, tempValues):
        while instruction > 0:
            row[index] = 1
            tempValues.append(index)

            index += 1
            instruction -= 1

        return row, tempValues

    def canAddInstruction(self, row, instruction, index) -> bool:
        while instruction > 0:
            try:
                row[index]
            except:
                return False

            if row[index] != 0:
                return False

            instruction -= 1
            index += 1

        return True

    def getPersistentRow(self, row, rowInstructions):
        instPower = sum(rowInstructions) + len(rowInstructions) - 1
        subFromRow = len(row) - instPower

        index = 0
        for inst in rowInstructions:
            if subFromRow >= inst:
                break
            else:
                i = index + subFromRow
                toAdd = inst - subFromRow

                while toAdd:
                    row[i] = 1
                    i += 1
                    toAdd -= 1
                

            index += inst + 1

        return row

    def __addValuesToMatrix(self, indices):
        for index in indices:
            self.matrix[index] = 1

    def solve(self):
        renderer = BoardRenderer((self.rows, self.columns))

        renderer.setMatrix(self.matrix)

        renderer.clock.tick(60)

        renderer.updateWindow()

        print(self.inst)

        print('rows')
        for i in range(0, self.rows):
            self.matrix[i] = self.getPersistentRow(self.matrix[i,], self.inst[0][i])

        print('columns')
        for i in range(0, self.columns):
            self.matrix[:,i] = self.getPersistentRow(self.matrix[:, i], self.inst[1][i])

        renderer.setMatrix(self.matrix)

        print(self.matrix)

        while True:
            renderer.clock.tick(60)
            renderer.updateWindow()

if __name__ == '__main__':
    NonogramSolver(NonogramGenerator().getNng((20, 20))[1]).solve()