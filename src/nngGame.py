from pygame import display
import pygame
from pygame.surface import Surface
import numpy as np
from nngGenerator import NonogramGenerator

pygame.font.init()

FPS = 60

BOARD_WIDTH, BOARD_HEIGHT = 600, 600 # make this divisble by board (x, y)

GAME_AREA_MARGIN = 150
GAME_AREA_WIDTH, GAME_AREA_HEIGHT = BOARD_WIDTH + GAME_AREA_MARGIN, BOARD_HEIGHT + GAME_AREA_MARGIN

WINDOW_WIDTH, WINDOW_HEIGHT = GAME_AREA_WIDTH, GAME_AREA_HEIGHT + 100

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
BOARD_BORDER_COLOR = (127, 127, 0)

CELL_SIZE = (0, 0)
DIMENSIONS = (0, 0)

FONT = pygame.font.SysFont('lucidaconsole', 14)

class Renderer():
    def drawWindow(self, window:Surface, gameArea:Surface, board:Surface, instructions = None):
        board = Board().addBorder(board, (127, 127, 0), 4)

        gameArea.fill(WHITE)
        
        if instructions != None: 
            textRenderer = TextRenderer()
            instructionText = textRenderer.generateInstructionText(instructions)
            gameArea = textRenderer.setInstructionText(instructionText, gameArea)
            gameArea.blit(board, (GAME_AREA_WIDTH - BOARD_WIDTH, GAME_AREA_HEIGHT - BOARD_HEIGHT))

        else:
            gameArea.blit(board, (0, 0))

        window.fill(WHITE)
        window.blit(gameArea, (0, 0))

        display.update()

    def addBorder(self, surf:Surface, color, size, top = 1, right = 1, bottom = 1, left = 1):
        surfW, surfH = surf.get_size()

        borderH = Surface((surfW, size))
        borderV = Surface((size, surfH))

        borderH.fill(color)
        borderV.fill(color)

        if top: surf.blit(borderH, (0, 0))
        if bottom: surf.blit(borderH, (0, surfH - size))
        if left: surf.blit(borderV, (0, 0))
        if right: surf.blit(borderV, (surfW - size, 0))

        return surf

class Board(Renderer):
    surface = Surface((BOARD_WIDTH, BOARD_HEIGHT))

    def generateBoard(self, size):
        x, y = size
        squareW, squareH = BOARD_WIDTH / x, BOARD_HEIGHT / y

        for i in range(0, x):
            for j in range(0, y):
                square = Surface((squareW, squareH))
                square.fill(WHITE)

                square = self.addBorder(square, BLACK, 1)

                self.surface.blit(square, (i * squareW, j * squareH))

        return self.surface, (squareW, squareH)

    def updateCell(self, pos, type):
        cellPos = (pos[0] * CELL_SIZE[0], pos[1] * CELL_SIZE[1])

        cell = Surface(CELL_SIZE)

        if type == -1:
            cell.fill((255, 0, 0))
            border = BLACK
        
        elif type == 0:
            cell.fill(WHITE)
            border = BLACK

        elif type == 1:
            cell.fill(BLACK)
            border = WHITE

        cell = self.addBorder(cell, border, 1)

        self.surface.blit(cell, cellPos)

        return self.surface

    def determineCell(self, pos):
        width, height = 0, 0
        x, y = -1, -1

        if(pos[0] == 0):
            x = 0

        else:
            while width / pos[0] < 1:
                width = width + CELL_SIZE[0]
                x = x + 1

        if(pos[1] == 0):
            y = 0

        else:
            while height / pos[1] < 1:
                height = height + CELL_SIZE[1]
                y = y + 1

        return (x, y)

    def handleBoardClicks(self, button, cell, board, matrix, action):
        matrixValue = matrix[cell[1]][cell[0]]

        valueToSet = None

        if button == 1:
            if matrixValue == 1:        valueToSet = 0

            else:                       valueToSet = 1

        if button == 2:
            valueToSet = 0

        if button == 3:
            if matrixValue == -1:       valueToSet = 0

            else:                       valueToSet = -1


        if action == None:
            action = valueToSet
            
        if action == valueToSet:
            matrix[cell[1]][cell[0]] = valueToSet
            board = self.updateCell(cell, valueToSet)

        return board, matrix, action

def generateNng(size):
    return NonogramGenerator().getNng(size)

class TextRenderer(Renderer):
    def generateInstructionText(self, instructions):
        xInstructions, yInstructions = instructions
        xText, yText = list(), list()

        for x in range(0, DIMENSIONS[0]):
            text = ""

            if len(xInstructions[x]) != 0:
                for number in xInstructions[x]:
                    text += str(number) + ' '

                xText.append(text[0:-1])
            
            else:
                xText.append("")

        for y in range(0, DIMENSIONS[1]):
            text = ""

            if len(yInstructions[y]) != 0:
                for number in yInstructions[y]:
                    text = text + str(number) + ' '

                yText.append(text[0:-1])
            
            else:
                yText.append("")

        return (xText, yText)

    def createXInstructionSurface(self, xText):
        textSurfaces = list[Surface]()
        width = 0

        for text in xText:
            surf = FONT.render(text, 1, BLACK)
            textSurfaces.append(surf)

            if width < surf.get_width(): width = surf.get_width() + 10

        xTextSurf = Surface((width, BOARD_HEIGHT))
        xTextSurf.fill(WHITE)
        xTextSurf = self.addBorder(xTextSurf, BOARD_BORDER_COLOR, 4, right = 0)

        cellPos = 0

        for surf in textSurfaces:
            xTextSurf.blit(surf, (width - surf.get_width() - 2, (CELL_SIZE[1] / 2 + cellPos - surf.get_height() / 2 + 2)))
            
            border = Surface((width, 2))
            border.fill(BOARD_BORDER_COLOR)
            
            xTextSurf.blit(border, (0, cellPos + CELL_SIZE[1] - 1))
            
            cellPos = cellPos + CELL_SIZE[1]

        return xTextSurf, width

    def createVerticalTextSurface(self, text):
        sampleChar = FONT.render('AA', 1, BLACK)

        charWidth = sampleChar.get_width()
        charHeight = sampleChar.get_height()

        margin = 1

        vSurf = Surface((charWidth, (charHeight + margin) * len(text)))
        vSurf.fill(WHITE)

        for letter, i in zip(text, range(len(text))):
            surf = FONT.render(letter, 1, BLACK)
            vSurf.blit(surf, (0, charHeight * i))

        return vSurf

    def createYInstructionSurface(self, yText):
        textSurfaces = list[Surface]()
        height = 0

        for text in yText:
            surf = self.createVerticalTextSurface(text)
            textSurfaces.append(surf)

            if height < surf.get_height(): height = surf.get_height() + 10

        yTextSurf = Surface((BOARD_WIDTH, height))
        yTextSurf.fill(WHITE)
        yTextSurf = self.addBorder(yTextSurf, BOARD_BORDER_COLOR, 4, bottom = 0)

        cellPos = 0

        for surf in textSurfaces:
            yTextSurf.blit(surf, ((CELL_SIZE[1] / 2 + cellPos - surf.get_width() / 2 + 2), height - surf.get_height()))
            
            border = Surface((2, height))
            border.fill(BOARD_BORDER_COLOR)
            
            yTextSurf.blit(border, (cellPos + CELL_SIZE[0] - 1, 0))
            
            cellPos = cellPos + CELL_SIZE[1]

        return yTextSurf, height

    def setInstructionText(self, instructionText, gameArea:Surface):
        boardBorder = GAME_AREA_MARGIN

        xTextSurf, width = self.createXInstructionSurface(instructionText[0])
        gameArea.blit(xTextSurf, (boardBorder - width, boardBorder))

        yTextSurf, height = self.createYInstructionSurface(instructionText[1])
        gameArea.blit(yTextSurf, (boardBorder, boardBorder - height))

        return gameArea

class Game():
    def checkIfWin(self, solution:np.matrix, matrix:np.matrix):
        for row in range(0, matrix.shape[0]):
            for column in range(0, matrix.shape[1]):
                if matrix[row][column] == -1:
                    matrix[row][column] = 0

        if not (False in np.equal(solution, matrix)):
            return True
        else:
            return False

    def startGame(self, dimensions):
        board = Board()
        boardMatrix = np.zeros(dimensions)
        solution, instructions = generateNng(dimensions)

        global CELL_SIZE, DIMENSIONS

        DIMENSIONS = dimensions

        board, CELL_SIZE = Board().generateBoard(dimensions)
        
        gameArea = Surface((GAME_AREA_WIDTH, GAME_AREA_HEIGHT))
        window = display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))

        renderer = Renderer()
        renderer.drawWindow(window, gameArea, board, instructions)

        return board, gameArea, window, boardMatrix, instructions, solution

def modal(message):
    pass

def main():
    game, boardFuncs = Game(), Board()
    board, gameArea, window, boardMatrix, instructions, solution = game.startGame((10, 10))

    paintedCells = list()

    run = True
    clock = pygame.time.Clock()
    mouseAction = None

    while(run):
        renderer = Renderer()

        clock.tick(FPS)
        draw = False

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()

            if event.type == pygame.MOUSEBUTTONUP:
                paintedCells = list()
                mouseAction = None

                x, y = pygame.mouse.get_pos()

                if(GAME_AREA_HEIGHT < y and game.checkIfWin(solution, boardMatrix)):
                    print('You win!')

                elif(GAME_AREA_HEIGHT < y):
                    print('You don\'t win :(')

            if event.type == pygame.MOUSEBUTTONDOWN or len(paintedCells) > 0:
                x, y = pygame.mouse.get_pos()

                dToBoard = (GAME_AREA_WIDTH - BOARD_WIDTH, GAME_AREA_HEIGHT - BOARD_HEIGHT)
                x, y = x - dToBoard[0], y - dToBoard[1]
                
                if not(x >= 0 and 0 <= y <= BOARD_HEIGHT):
                    paintedCells = list()
                    mouseAction = None
                    break

                buttons = pygame.mouse.get_pressed()

                button = None

                for b, i in zip(reversed(buttons), reversed(range(len(buttons)))):
                    if b: button = i + 1

                cell = boardFuncs.determineCell((x, y))
                
                if (x >= 0 and 0 <= y <= BOARD_HEIGHT):
                    if(not (cell in paintedCells) and button != None):
                        paintedCells.append(cell)

                        board, boardMatrix, mouseAction = boardFuncs.handleBoardClicks(button, cell, board, boardMatrix, mouseAction)
                        draw = True

        if draw: renderer.drawWindow(window, gameArea, board, instructions)

if __name__ == '__main__':
    main()

class BoardRenderer(Renderer):
    def __init__(self, size):
        self.size = size

        self.board = Board()
        self.boardMatrix = np.zeros(size)
        global CELL_SIZE
        self.boardSurf, CELL_SIZE = Board().generateBoard(size)

        self.gameArea = Surface((BOARD_WIDTH, BOARD_HEIGHT))
        self.window = None

        self.clock = pygame.time.Clock()

    def updateWindow(self):
        if self.window == None:
            self.window = display.set_mode((BOARD_WIDTH, BOARD_WIDTH))

        self.board.drawWindow(self.window, self.gameArea, self.boardSurf)

        for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()

    def setMatrix(self, matrix):
        for row in range(len(matrix)):
            for column in range(len(matrix[row])):
                self.boardMatrix[row][column] = matrix[column][row]
                self.boardSurf = self.board.updateCell((row, column), self.boardMatrix[row][column])
