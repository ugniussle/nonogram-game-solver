import unittest
import nngSolver
import numpy as np

class TestCheckIfSolved(unittest.TestCase):
    def testZerosMatrixAndEmptyInstructions(self):
        m = np.zeros((5, 5))
        inst = list()

        for i in range(2):
            inst.append(list())
            for j in range(5):
                inst[i].append(list())

        obj = nngSolver.NonogramSolver(inst)

        self.assertEqual(obj.checkIfSolved(m, inst), True)

    def testOnesMatrixAndFullInstructions(self):
        m = np.ones((5, 5))
        inst = list()

        for i in range(2):
            inst.append(list())
            for j in range(5):
                inst[i].append(list())
                inst[i][j].append(5)

        obj = nngSolver.NonogramSolver(inst)

        self.assertEqual(obj.checkIfSolved(m, inst), True)

    def testEmptyMatrixAndEmptyInstructions(self):
        m = np.empty((5, 5))
        inst = list()

        for i in range(2):
            inst.append(list())
            for j in range(5):
                inst[i].append(list())

        obj = nngSolver.NonogramSolver(inst)

        self.assertEqual(obj.checkIfSolved(m, inst), True)

    def testOnesRowMatrixAndCorrectInstructions(self):
        m = np.zeros((5, 5))
        inst = list()

        for row in range(m.shape[0]):
            m[row, 0] = 1

        inst.append(list())
        for j in range(5):
            inst[0].append(list())
            inst[0][j].append(1)

        inst.append(list())
        inst[1].append(list())
        inst[1][0].append(5)

        for j in range(1, 5):
            inst[1].append(list())

        obj = nngSolver.NonogramSolver(inst)

        self.assertEqual(obj.checkIfSolved(m, inst), True)

