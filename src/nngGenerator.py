import random
import numpy as np

class NonogramGenerator:
    matrixDensity = 0.65
    # add minimum and maximum density, make it random for more variety

    def generateInstructions(self, matrix):
        insRows = self.createOnesLengthsArrays(matrix)

        insCols = self.createOnesLengthsArrays(np.transpose(matrix))

        return insRows, insCols

    def createOnesLengthsArrays(self, matrix:np.ndarray):
        LengthsArrays = list()

        for i in range(0, matrix.shape[0]):
            acc = 0
            lengthsArray = list()

            for j in range(0, matrix.shape[1]):
                if(matrix[i][j] == 1):
                    acc = acc + 1

                elif(matrix[i][j] == 0 and acc > 0):
                    lengthsArray.append(acc)
                    acc = 0

            if acc > 0:
                lengthsArray.append(acc)

            LengthsArrays.append(lengthsArray)

        return LengthsArrays

    def generateMatrix(self, x, y):
        matrix = np.zeros((x, y))

        onesCount = 0

        for i in range(x):
            for j in range(y):
                if (random.randint(1, 100) / 100) < self.matrixDensity:
                    matrix[i][j] = 1
                    onesCount = onesCount + 1

        return matrix

    def getNng(self, size):
        matrix = self.generateMatrix(size[0], size[1])
        instructions = self.generateInstructions(matrix)

        return matrix, instructions
